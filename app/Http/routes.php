<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	return view('index');
});

Route::get('login', ['as'=>'login',function() {
	return view('auth.login');
}]);

Route::post('auth/login', ['as'=>'logincheck', 'uses'=>'Auth\AuthController@postLogin']);

Route::get('home', ['as'=>'home',function() {
	return view('home');
}]);

Route::get('registration',['as'=>'registration', function(){
	return view('register.registration');
}]);

Route::post('register',['as'=>'register', 'uses'=>'registration\register@create']);