<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Registration</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script>
	function validatePassword(){
		var password = document.getElementById("password");
		var confirm_password = document.getElementById("confirm_password");
	  if(password.value != confirm_password.value) {
	    confirm_password.setCustomValidity("Passwords Don't Match");
	  } else {
	    confirm_password.setCustomValidity('');
	  }
	}

	</script>
</head>
<body>
	<div class="wrapper">
		<div class="regisform">
			<form action="{{URL::route('register')}}" method="post" accept-charset="utf-8">
				<label>First Name : <input type="text" name="firstName" value="" required></label><br><br>
				<label>Last Name : <input type="text" name="lastName" value="" required></label><br><br>
				<label>Email : <input type="email" name="email" value="" required></label><br><br>
				<label>Password : <input type="password" id="password" name="password" value="" required onchange="validatePassword()"></label><br><br>
				<label>Re-type Password : <input type="password" id="confirm_password" name="confirm_password" value="" required onkeyup="validatePassword()"></label><br><br>
				<input type="hidden" name="_token" value="{{csrf_token()}}" placeholder="">
				<input type="submit" name="registerBtn" value="Register">
			</form>
		</div>
	</div>
</body>
</html>