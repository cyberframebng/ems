<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Login</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<div class="wrapper">
	@if(Session::has('message'))
		{{Session::get('message')}}<br>
	@endif
		<div class="loginform">
			<form action="{{URL::route('logincheck')}}" method="post" accept-charset="utf-8">
				<label>Email ID : <input type="email" name="email" value=""></label><br><br>
				<label>Password : <input type="password" name="password" value=""></label><br><br>
				<input type="hidden" name="_token" value="{{csrf_token()}}" placeholder="">
				<input type="submit" name="loginsbt" value="Login">
			</form>
		</div>
	</div>
</body>
</html>
